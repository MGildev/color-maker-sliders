//
//  ViewController.swift
//  Color Maker Sliders
//
//  Created by Miguel Gil Aleixandre on 17/5/17.
//  Copyright © 2017 Miguel Gil Aleixandre. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var colorView: UIView!
    var currentRed : Float = 0.0
    var currentGreen : Float = 0.0
    var currentBlue : Float = 0.0

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func sliderTouched(_ sender: UISlider) {
        switch sender.restorationIdentifier! {
            case "blueSlider":
                currentBlue = sender.value
                break
            case "redSlider":
                currentRed = sender.value
                break
            case "greenSlider":
                currentGreen = sender.value
                break
            default:
                break
        }
        colorView.backgroundColor = UIColor(colorLiteralRed: currentRed, green: currentGreen, blue: currentBlue, alpha: 1)

    }
}

